﻿const mysql = require("mysql2");
const express = require("express");
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
 
const app = express();
const urlencodedParser = bodyParser.urlencoded({extended: false});
const http = require("http");
const hostname = '127.0.0.1';
const port = 3000;


const pool = mysql.createPool({
  connectionLimit: 5,
  host: "localhost",
  user: "root",
  database: "users",
  password: "gjkysqgbpltw"
});
//для подключения таблицы стилей
app.use(express.static("."));

global.login = "";
 
app.set("view engine", "hbs");

// открываем форму для входа
app.get("/", function(req, res){
    res.render("autorization.hbs");
});

// открываем форму для добавления данных
app.get("/create", function(req, res){
    res.render("registration.hbs");
});
//проверка логина и пароля входящего
app.post("/", urlencodedParser, (req, res) => {
    const login = req.body.login;
    const password1 = req.body.password;
    const password = crypto.createHash('SHA1').update(password1).digest('hex');
    pool.query("SELECT id FROM loginpasp WHERE login=? and password=?",[login, password], async(err,results) => {
	if (results.length>0) {
	    global.login=req.body.login;
	    res.redirect("/edit");
	}else{
	    console.log("Неправильное имя пользователя или пароль");
	}
    })
});
// проверяем логин на совпадение и получаем данные и добавляем их в БД 
app.post("/create", urlencodedParser, (req, res) => {
    let user = req.body;
    const login = req.body.login;
    global.login = req.body.login;
    pool.query("SELECT login FROM loginpasp WHERE login=?", user.login, async(err,results) => {
	if(err) throw err;
	if(results.length>0) {
	    //res.render('registration.hbs',
	}else{
    	    if(!req.body) return res.sendStatus(400);
    	    const userName = req.body.userName;
    	    const userFam = req.body.userFam;
    	    const userOtch = req.body.userOtch;
    	    const pasSeries = req.body.pasSeries;
    	    const pasNumber = req.body.pasNumber;
     	    const pasDataV = req.body.pasDataV;
    	    const pasVydan = req.body.pasVydan;
    	    const login = req.body.login;
    	    const password1 = req.body.password;
	    const password = crypto.createHash('SHA1').update(password1).digest('hex');
    	    pool.query("INSERT INTO loginpasp (userName, userFam, userOtch, pasSeries, pasNumber, pasDataV, pasVydan, login, password) VALUES (?,?,?,?,?,?,?,?,?)", [userName, userFam, userOtch, pasSeries, pasNumber, pasDataV, pasVydan, login, password], function(err, data) {
      		if(err) return console.log(err);
      		res.redirect("/edit");
    	    });
	}
    })
});
 
// по login получаем данные пользователя из БД
app.get("/edit", function(req, res){
  const login = global.login;
  pool.query("SELECT * FROM loginpasp WHERE login=?", [login], function(err, data) {
    if(err) return console.log(err);
     res.render("useroffice.hbs", {
        user: data[0]
    });
  });
});
// получаем отредактированные данные и отправляем их в БД
app.post("/edit", urlencodedParser, function (req, res) {
         
  if(!req.body) return res.sendStatus(400);
  const userName = req.body.userName;
  const userFam = req.body.userFam;
  const userOtch = req.body.userOtch;
  const pasSeries = req.body.pasSeries;
  const pasNumber = req.body.pasNumber;
  const pasDataV = req.body.pasDataV;
  const pasVydan = req.body.pasVydan;
  const id = req.body.id;
  pool.query("UPDATE loginpasp SET userName=?, userFam=?, userOtch=?, pasSeries=?, pasNumber=?, pasDataV=?, pasVydan=? WHERE id=?", [userName, userFam, userOtch, pasSeries, pasNumber, pasDataV, pasVydan, id], function(err, data) {
    if(err) return console.log(err);
  });
});
// по login получаем данные пользователя из БД
app.get("/anketa", function(req, res){
  const login = global.login;
  pool.query("SELECT * FROM loginpasp WHERE login=?", [login], function(err, data) {
    if(err) return console.log(err);
     res.render("anketa.hbs", {
        user: data[0]
    });
  });
});
// 
app.post("/anketa", urlencodedParser, (req, res) => {
    let user = req.body;
    const login = req.body.login;
    const sport = req.body.q1;
    const cook = req.body.q2;
    const rest = req.body.q3;
    const pet = req.body.q4;
    const banana = req.body.ba;
    const orange = req.body.ap;
    const apple = req.body.ib;
    const pear = req.body.gr;
    global.login = req.body.login;
    pool.query("SELECT login FROM anketa WHERE login=?", user.login, async(err,results) => {
	if(err) throw err;
	if(results.length>0) {
	    pool.query("UPDATE anketa SET sport=?, cook=?, rest=?, pet=?, banana=?, orange=?, apple=?, pear=? WHERE login=?", [sport, cook, rest, pet, banana, orange, apple, pear, login], function(err, data) {
   	        if(err) return console.log(err);
      		res.redirect("/edit");
		});
	}else{
    	    pool.query("INSERT INTO anketa (sport, cook, rest, pet, banana, orange, apple, login, pear) VALUES (?,?,?,?,?,?,?,?,?)", [sport, cook, rest, pet, banana, orange, apple, login, pear], function(err, data) {
      		if(err) return console.log(err);
      		res.redirect("/edit");
    	    });
	}
    })
});

 
app.listen(3000, function(){
  console.log("Сервер ожидает подключения...");
});